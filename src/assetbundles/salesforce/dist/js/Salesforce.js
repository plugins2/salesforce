/**
 * Salesforce plugin for Craft CMS
 *
 * Salesforce JS
 *
 * @author    Craig Roy
 * @copyright Copyright (c) 2020 Craig Roy
 * @link      http://croy.solutions/
 * @package   Salesforce
 * @since     1.0.0
 */
