<?php
/**
 * Salesforce plugin for Craft CMS 3.x
 *
 * Hooks into crafts contact submit event to send lead to salesforce
 *
 * @link      http://croy.solutions/
 * @copyright Copyright (c) 2020 Craig Roy
 */

namespace croy37\salesforce\models;

use croy37\salesforce\Salesforce;

use Craft;
use craft\base\Model;

/**
 * Salesforce Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Craig Roy
 * @package   Salesforce
 * @since     1.0.0
 */
class Settings extends Model
{
    /**
     * @var string
     */
    public $salesforceAccountId = '';

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['salesforceAccountId', 'string'],
        ];
    }

    /**
     * @return string the parsed secret key (e.g. 'XXXXXXXXXXX')
     */
    public function getSalesforceAccountId(): string
    {
        $appSecrets = getenv('APP_SECRETS');

        if ($appSecrets !== false) {
            $secrets = json_decode(file_get_contents(getenv('APP_SECRETS')), true);
            return $secrets['CUSTOM']['SALESFORCE_ACCOUNT_ID'];
        }

        return Craft::parseEnv($this->salesforceAccountId);
    }
}
