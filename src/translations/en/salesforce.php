<?php
/**
 * Salesforce plugin for Craft CMS 3.x
 *
 * Hooks into crafts contact submit event to send lead to salesforce
 *
 * @link      http://croy.solutions/
 * @copyright Copyright (c) 2020 Craig Roy
 */

/**
 * Salesforce en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('salesforce', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Craig Roy
 * @package   Salesforce
 * @since     1.0.0
 */
return [
    'Salesforce plugin loaded' => 'Salesforce plugin loaded',
];
